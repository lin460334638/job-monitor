package com.bosssoft.hr.submissionTool.serviceImpl;

import com.bosssoft.hr.submissionTool.domain.Admin;
import com.bosssoft.hr.submissionTool.domain.Email;
import com.bosssoft.hr.submissionTool.domain.Picture;
import com.bosssoft.hr.submissionTool.domain.User;
import com.bosssoft.hr.submissionTool.service.EmailService;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.*;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Iterator;
import java.util.Properties;
import java.util.List;
public class EmailSerivceImpl implements EmailService {

    @Override
    public Message createMail(Session session,Admin admin,User user,int status) {
        try{
            //创建邮件
            MimeMessage message = new MimeMessage(session);
            //设置邮件的基本信息
            message.setFrom(new InternetAddress(admin.getEmail().getAddress()));
            if(status==0)
                message.setRecipient(Message.RecipientType.TO, new InternetAddress(admin.getEmail().getAddress()));
            else
                message.setRecipient(Message.RecipientType.TO, new InternetAddress(user.getEmail().getAddress()));
     //       message.setRecipient(Message.RecipientType.CC, new InternetAddress(user.getEmail().getCarbonCopy()));
            message.setSubject(admin.getEmail().getTitle());
            //正文
            MimeBodyPart text = new MimeBodyPart();
            text.setContent(admin.getEmail().getContent(),"text/html;charset=UTF-8");


            //描述关系:正文和图片
            MimeMultipart mp1 = new MimeMultipart();
            mp1.addBodyPart(text);
            if(admin.getEmail().getPictureList()!=null){
                for(Picture picture: admin.getEmail().getPictureList()){
                    //图片
                    MimeBodyPart image = new MimeBodyPart();
                    image.setDataHandler(new DataHandler(new FileDataSource(picture.getPath())));
                    image.setContentID(picture.getPictureId());
                    mp1.addBodyPart(image);
                }
            }

            mp1.setSubType("related");
            //描述关系:正文和附件
           MimeMultipart mp2 = new MimeMultipart();
            if(admin.getEmail().getEnclosureList()!=null) {
                for (String enclosure : admin.getEmail().getEnclosureList()) {
                    //附件
                    MimeBodyPart attach = new MimeBodyPart();
                    DataHandler dh = new DataHandler(new FileDataSource(enclosure));
                    attach.setDataHandler(dh);
                    attach.setFileName(MimeUtility.encodeText(dh.getName()));
                    mp2.addBodyPart(attach);
                }
            }
            MimeBodyPart content = new MimeBodyPart();
            content.setContent(mp1);
            mp2.addBodyPart(content);
            mp2.setSubType("mixed");

            message.setContent(mp2);
            message.saveChanges();
            message.writeTo(new FileOutputStream("E:\\MixedMail.eml"));
            //返回创建好的的邮件
            return message;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }

    }
    @Override
    public void sendMessage(Admin admin,User user,int status) {
        try{
            Properties prop = new Properties();
            prop.setProperty("mail.host", admin.getEmail().getHost());
            prop.setProperty("mail.transport.protocol", admin.getEmail().getProtocol());
            prop.setProperty("mail.smtp.auth", admin.getEmail().getAuth());
            Session session = Session.getInstance(prop);
            //开启Session的debug模式，这样就可以查看到程序发送Email的运行状态
            session.setDebug(true);
            //2、通过session得到transport对象
            Transport ts = session.getTransport();
            //3、连上邮件服务器
            ts.connect(admin.getEmail().getHost(), admin.getEmail().getAddress(),admin.getEmail().getPassword());

            //4、创建邮件
            Message message = createMail(session, admin, user, status);
                //5、发送邮件
            ts.sendMessage(message, message.getAllRecipients());
            ts.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

}
