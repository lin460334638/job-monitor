package com.bosssoft.hr.submissionTool.service;

import com.bosssoft.hr.submissionTool.domain.Admin;
import com.bosssoft.hr.submissionTool.domain.Email;
import com.bosssoft.hr.submissionTool.domain.User;
import java.util.List;
import javax.mail.Message;
import javax.mail.Session;

public interface EmailService {

    Message createMail(Session session,Admin admin,User user,int status);

    void sendMessage(Admin admin,User user,int status);


}
