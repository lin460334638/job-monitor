package com.bosssoft.hr.submissionTool;

import com.bosssoft.hr.submissionTool.domain.Admin;
import com.bosssoft.hr.submissionTool.domain.Email;
import com.bosssoft.hr.submissionTool.domain.Task;
import com.bosssoft.hr.submissionTool.domain.User;
import com.bosssoft.hr.submissionTool.service.EmailService;
import com.bosssoft.hr.submissionTool.service.FileService;
import com.bosssoft.hr.submissionTool.service.XMLService;
import com.bosssoft.hr.submissionTool.serviceImpl.EmailSerivceImpl;
import com.bosssoft.hr.submissionTool.serviceImpl.FileServiceImpl;
import com.bosssoft.hr.submissionTool.serviceImpl.XMLServiceImpl;

import java.util.List;
/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        String userPath = "E:\\IntelliJ IDEA\\JavaProjects\\submissionTool\\src\\main\\resources\\user_config.xml";
        String adminPath = "E:\\IntelliJ IDEA\\JavaProjects\\submissionTool\\src\\main\\resources\\admin_config.xml";
        String taskPath = "E:\\IntelliJ IDEA\\JavaProjects\\submissionTool\\src\\main\\resources\\task_config.xml";
        String emailPath = "E:\\IntelliJ IDEA\\JavaProjects\\submissionTool\\src\\main\\resources\\email_config.xml";
        EmailService emailService = new EmailSerivceImpl();
        XMLService xmlService = new XMLServiceImpl();
        FileService fileService = new FileServiceImpl();
        List<User> userList = xmlService.queryUserFromXML(userPath);
        Admin admin = xmlService.getAdminFromXML(adminPath);
        List<Task> taskList = xmlService.queryTaskFromXML(taskPath);
        admin.setTaskList(taskList);
    //    List<Email> emailList = xmlService.queryEmailFromXML(emailPath);
   //     emailService.sendMessage(admin,userList);
     //    fileService.searchFile(admin,userList);

    }
}
