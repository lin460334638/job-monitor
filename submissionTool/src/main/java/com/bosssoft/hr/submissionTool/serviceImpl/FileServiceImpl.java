package com.bosssoft.hr.submissionTool.serviceImpl;
import com.bosssoft.hr.submissionTool.domain.Admin;
import com.bosssoft.hr.submissionTool.domain.Email;
import com.bosssoft.hr.submissionTool.domain.User;
import com.bosssoft.hr.submissionTool.service.EmailService;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.bosssoft.hr.submissionTool.service.FileService;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.quartz.DailyTimeIntervalScheduleBuilder.dailyTimeIntervalSchedule;
import static org.quartz.DateBuilder.*;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

public class FileServiceImpl implements FileService{
    private static final Logger logger = LoggerFactory.getLogger(FileServiceImpl.class);
    public static List<User> submittedList = new ArrayList<>();
    @Override
    public void searchFile(Admin admin, List<User> userList, List<Email> emailList) {
        try{

            File file = new File(admin.getFilePath());
            if(!file.exists()){
                logger.info("file is not exist,create file...");
                file.mkdir();
                logger.info("create file succeed");
            }
            String [] fileArr = file.list();
            if(fileArr.length==0 ||fileArr==null){
                logger.info("this file is empty");
            }
            int  i;
            String content ="<p>作业提交情况</p>";
            EmailService emailService = new EmailSerivceImpl();
            for(User user: userList) {
                for (i = 0; i < fileArr.length; i++) {
                    if (fileArr[i].contains(user.getUserName())) {
                        logger.info("fought user file,userName is " + user.getUserName());
                        admin.getEmail().setTitle(emailList.get(0).getTitle());
                        content +="<p>"+user.getUserName()+ emailList.get(0).getContent()+"</p>";
                        break;
                    }
                }
                if(i==fileArr.length)
                    logger.info("cannot fought user file,userName is " + user.getUserName());
            }
            admin.getEmail().setContent(content);
            User user = new User();
            emailService.sendMessage(admin, user, 0);
        }catch(Exception e){
            logger.error(e.getMessage());
        }
    }

    @Override
    public void setScanner(){
        try {
            //创建scheduler
            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

            //定义一个Trigger
            Trigger trigger = newTrigger().withIdentity("trigger1", "group1") //定义name/group
                    .startNow()//一旦加入scheduler，立即生效
                    .withSchedule(dailyTimeIntervalSchedule()
                            .startingDailyAt(TimeOfDay.hourAndMinuteOfDay(14, 32)) //第天9：00开始
                            .endingDailyAt(TimeOfDay.hourAndMinuteOfDay(20, 45)) //16：00 结束
                            .onDaysOfTheWeek(MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY) //周一至周五执行
                            .withIntervalInSeconds(20))//每间隔1小时执行一次
                    .build();

            //定义一个JobDetail
            JobDetail job = JobBuilder.newJob(ScannerJobServiceImpl.class) //定义Job类为HelloQuartz类，这是真正的执行逻辑所在
                    .withIdentity("job1", "group1") //定义name/group
                    .usingJobData("name", "quartz") //定义属性
                    .build();

            //加入这个调度
            scheduler.scheduleJob(job, trigger);

            //启动之
            scheduler.start();
            while (true) {
                Thread.sleep(14400000);

            }
           //运行一段时间后关闭

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
