package com.bosssoft.hr.submissionTool.serviceImpl;

import com.bosssoft.hr.submissionTool.domain.Admin;
import com.bosssoft.hr.submissionTool.domain.Email;
import com.bosssoft.hr.submissionTool.domain.Task;
import com.bosssoft.hr.submissionTool.domain.User;
import com.bosssoft.hr.submissionTool.service.XMLService;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.dom4j.Document;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.util.List;

public class XMLServiceImpl implements XMLService {

    @Override
    public List<User> queryUserFromXML(String path) {
        XStream xstream = new XStream(new DomDriver());
        xstream.alias("User",User.class);
        List<User> userList = (List<User>)xstream.fromXML(readXml(path));
        return userList;
    }
    @Override
    public Admin getAdminFromXML(String path) {
        XStream xstream = new XStream(new DomDriver());
        xstream.alias("Admin",Admin.class);
        Admin admin = (Admin)xstream.fromXML(readXml(path));
        return admin;
    }

    @Override
    public List<Task> queryTaskFromXML(String path) {
        XStream xstream = new XStream(new DomDriver());
        xstream.alias("Task", Task.class);
        List<Task> taskList = (List<Task>)xstream.fromXML(readXml(path));
        return taskList;
    }

    @Override
    public List<Email> queryEmailFromXML(String path) {
        XStream xstream = new XStream(new DomDriver());
        xstream.alias("Email", Email.class);
        List<Email> emailList = (List<Email>)xstream.fromXML(readXml(path));
        return emailList;
    }

    public String readXml(String path) {
        try{
            SAXReader saxReader=new SAXReader();   //"E:\\IntelliJ IDEA\\JavaProjects\\submissionTool\\src\\main\\resources\\user_config.xml"
            Document document = saxReader.read(new File(path));
            String xmlString=document.asXML();   //将xml内容转化为字符串
            return xmlString;
        }catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
