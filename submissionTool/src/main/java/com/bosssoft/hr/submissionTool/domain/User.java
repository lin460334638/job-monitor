package com.bosssoft.hr.submissionTool.domain;
import java.util.List;
public class User {

    private Integer userId;

    private Email email;

    private String userName;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Email getEmail() {
        return email;
    }

    public void setEmail(Email email) {
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof User) {
            User user= (User) obj;
            return this.getUserName().equals(((User) obj).getUserName());
        }
        return false;
    }

}
