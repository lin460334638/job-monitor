package com.bosssoft.hr.submissionTool.domain;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/*
**Eamil类 邮件实体
* sender 发件人地址
* recipient 收件人地址
* title 文档标题
* content 正文
* host 服务器
* protocol 协议
* password 邮箱密码
* pictureList 图片列表
* enclosureList 附件列表
* auth 认证
* */
public class Email implements Serializable {

    private String address;

    private String title;

    private String content;

    private String password;

    private String carbonCopy;

    private String host;

    private String protocol;

    private String auth;

    private List<Picture> pictureList;

    private List<String> enclosureList ;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public void setPictureList(List<Picture> pictureList) {
        this.pictureList = pictureList;
    }

    public List<Picture> getPictureList() {
        return pictureList;
    }

    public List<String> getEnclosureList() {
        return enclosureList;
    }

    public void setEnclosureList(List<String> enclosureList) {
        this.enclosureList = enclosureList;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getAuth() {
        return auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }

    public String getCarbonCopy() {
        return carbonCopy;
    }

    public void setCarbonCopy(String carbonCopy) {
        this.carbonCopy = carbonCopy;
    }
}
