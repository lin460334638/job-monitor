package com.bosssoft.hr.submissionTool.service;

import com.bosssoft.hr.submissionTool.domain.Admin;
import com.bosssoft.hr.submissionTool.domain.Email;
import com.bosssoft.hr.submissionTool.domain.User;
import java.util.List;
public interface FileService {

    void searchFile(Admin admin, List<User> userList, List<Email> emailList);

    void setScanner();
}
