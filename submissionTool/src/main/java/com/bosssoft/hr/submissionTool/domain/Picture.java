package com.bosssoft.hr.submissionTool.domain;

public class Picture {
    private String pictureId;
    private String path;

    public String getPictureId() {
        return pictureId;
    }

    public void setPictureId(String pictureId) {
        this.pictureId = pictureId;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
