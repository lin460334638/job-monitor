package com.bosssoft.hr.submissionTool.serviceImpl;

import com.bosssoft.hr.submissionTool.domain.Admin;
import com.bosssoft.hr.submissionTool.domain.Email;
import com.bosssoft.hr.submissionTool.domain.User;
import com.bosssoft.hr.submissionTool.service.FileService;
import com.bosssoft.hr.submissionTool.service.XMLService;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.Date;
import java.util.List;

public class ScannerJobServiceImpl implements Job {
    String adminPath = "E:\\IntelliJ IDEA\\JavaProjects\\submissionTool\\src\\main\\resources\\admin_config.xml";
    String userPath = "E:\\IntelliJ IDEA\\JavaProjects\\submissionTool\\src\\main\\resources\\user_config.xml";
    String taskPath = "E:\\IntelliJ IDEA\\JavaProjects\\submissionTool\\src\\main\\resources\\task_config.xml";
    String emailPath = "E:\\IntelliJ IDEA\\JavaProjects\\submissionTool\\src\\main\\resources\\email_config.xml";
    public void execute(JobExecutionContext context) throws JobExecutionException {
        FileService fileService = new FileServiceImpl();
        XMLService xmlService = new XMLServiceImpl();
        Admin admin = xmlService.getAdminFromXML(adminPath);
        List<User> userList = xmlService.queryUserFromXML(userPath);
        List<Email> emailList = xmlService.queryEmailFromXML(emailPath);
        fileService.searchFile(admin,userList,emailList);
    }
}
