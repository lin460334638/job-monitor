package com.bosssoft.hr.submissionTool.service;

import com.bosssoft.hr.submissionTool.domain.Admin;
import com.bosssoft.hr.submissionTool.domain.Email;
import com.bosssoft.hr.submissionTool.domain.Task;
import com.bosssoft.hr.submissionTool.domain.User;

import java.util.List;

public interface XMLService {

    List<User> queryUserFromXML(String path);

    Admin getAdminFromXML(String path);

    List<Task> queryTaskFromXML(String path);

    List<Email> queryEmailFromXML(String path);
}
